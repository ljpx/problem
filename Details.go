package problem

import (
	"fmt"

	"gitlab.com/ljpx/trace"
)

// Details is the main item of the package.  When serialized, it produces a
// valid RFC-7807 problem details structure.
type Details struct {
	trace.CorrelatableBase

	Type      string      `json:"type"`
	Title     string      `json:"title"`
	Detail    string      `json:"detail,omitempty"`
	Specifics interface{} `json:"specifics,omitempty"`
	Error     string      `json:"error,omitempty"`
}

var _ trace.Traceable = &Details{}

// Identify identifies the type.
func (d *Details) Identify() string {
	return "ProblemDetails"
}

// Describe describes the contents of the type.
func (d *Details) Describe() string {
	return fmt.Sprintf("%v (%v)", d.Title, d.Type)
}

// AttachError attaches an error to the calling Details object.
func (d *Details) AttachError(err error) {
	if err != nil {
		d.Error = err.Error()
	} else {
		d.Error = ""
	}
}

// Redact returns a redacted copy of the problem (no information is actually
// redacted).
func (d *Details) Redact() interface{} {
	cp := *d
	return &cp
}
