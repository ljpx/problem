package problem

import (
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/ljpx/id"
)

func TestDetailsJSON(t *testing.T) {
	// Arrange.
	id := id.New()
	err := errors.New("it is broken")

	problem := &Details{
		Type:   "https://example.com/generic-problem",
		Title:  "Generic Problem",
		Detail: "Generic Problem!",
		Specifics: map[string]string{
			"Hello": "World",
		},
	}

	problem.AddCorrelation("test", id)
	problem.AttachError(err)

	// Act.
	rawJSON, err := json.Marshal(problem.Redact())
	require.Nil(t, err)

	json := string(rawJSON)
	expectedJSON := fmt.Sprintf(`{"correlations":{"test":"%v"},"type":"https://example.com/generic-problem","title":"Generic Problem","detail":"Generic Problem!","specifics":{"Hello":"World"},"error":"it is broken"}`, id)

	// Assert.
	require.Equal(t, expectedJSON, json)
}

func TestDetailsAttachError(t *testing.T) {
	// Arrange.
	problem := &Details{}

	// Act.
	problem.AttachError(errors.New("test error"))

	// Assert.
	problem.Error = "test error"
}

func TestDetailsAttachNilError(t *testing.T) {
	// Arrange.
	problem := &Details{}
	problem.Error = "test error"

	// Act.
	problem.AttachError(nil)

	// Assert.
	problem.Error = ""
}

func TestDetailsShouldIdentifyAndDescribe(t *testing.T) {
	// Arrange.
	problem := &Details{
		Type:   "https://example.com/generic-problem",
		Title:  "Generic Problem",
		Detail: "Generic Problem!",
		Specifics: map[string]string{
			"Hello": "World",
		},
	}

	// Act.
	identify := problem.Identify()
	describe := problem.Describe()

	// Assert.
	require.Equal(t, "ProblemDetails", identify)
	require.Equal(t, "Generic Problem (https://example.com/generic-problem)", describe)
}
