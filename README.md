![](icon.png)

# problem

Package `problem` implements a slightly modified version of
[RFC-7807](https://tools.ietf.org/html/rfc7807) ("Problem Details").

## Usage Example

Usage is extremely simple:

```go
problem := &Details{
    Type:   "https://example.com/generic-problem",
    Title:  "Generic Problem",
    Detail: "Generic Problem!",
    Specifics: map[string]string{
        "Hello": "World",
    },
}

problem.AttachError(err)
problem.CorrelateWith(reqModel)
```

Functionality of around this package is enhanced somewhat by the
[`web`](https://gitlab.com/ljpx/web) package.